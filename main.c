// FFXI-Nix v0.1
/*this program launchest ffxi via ashita on linux
current state: uses a external shell script


Copyright (C) 2021 Luna Faris

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the i// FFXI-Nix v0.1
/*this program launchest ffxi via ashita on linux
current state: uses a external shell script


Copyright (C) 2021 Luna Faris

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA*/

// Headers

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>

// Random Variables
    char              textlab[64];
    char              aboutlab[100];
    int               textlength = 35;
    char              fullpath[150];
    char              command[200];
    const char*       folderpathx;
    char*             filename;

// Widgets
    GtkWidget *window;
    GtkWidget *vertbox;
    GtkWidget *checkwin;
    GtkWidget *checkwinbox;
    //GtkWidget *wineprefix;
    //GtkWidget *username;
    //GtkWidget *password;
    GtkWidget *textstuff;
    GtkWidget *pokeme;
    GtkWidget *toolbar;
    GtkWidget *menu;
    GtkWidget *filemenu;
    GtkWidget *filesubmenu;
    GtkWidget *aboutmenu;
    GtkWidget *aboutsubmenu;
    GtkWidget *closeswitch;
    GtkWidget *aboutswitch;
    GtkWidget *dialog;
    GtkWidget *image;
    

// Events
static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
   return FALSE;
}

static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}

// Launch
static void launch (GtkWidget *wid, GtkWidget *win)
{
  int checkfail;

  char *const args[] = { "/usr/bin/sh",".ffxi.sh",NULL };
  pid_t pid = fork() ;

  if (pid == 0)
  {
	  execv(args[0], args);
  }else{
	  waitpid( pid,&checkfail,0 ) ;
	  if (WEXITSTATUS(checkfail) != 0)
	  {
		  GtkWidget *dialog = NULL;
		  dialog = gtk_message_dialog_new (GTK_WINDOW (win), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "Could not properly launch FFXI with credentials.\n");
		  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
		  gtk_dialog_run (GTK_DIALOG (dialog));
		  gtk_widget_destroy (dialog);
	  }
  }
}

// Config
static void config (GtkWidget *wid, GtkWidget *win)
{
  int checkfail;

  char *const args[] = { "/usr/bin/sh",".ffxi-graphics.sh",NULL };

  pid_t pid = fork() ;

  if (pid == 0)
  {
	  execv(args[0], args);
      
  }else{
	  waitpid( pid,&checkfail,0 ) ;
	  if (WEXITSTATUS(checkfail) != 0)
	  {
		  GtkWidget *dialog = NULL;
		  dialog = gtk_message_dialog_new (GTK_WINDOW (win), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "Could not properly launch FFXIConfig.\n");
		  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
		  gtk_dialog_run (GTK_DIALOG (dialog));
		  gtk_widget_destroy (dialog);
	  }
    }
}

// Config XI
static void configxi (GtkWidget *wid, GtkWidget *win)
{
  int checkfail;

  char *const args[] = { "/usr/bin/sh",".ffxi-config.sh",NULL };

  pid_t pid = fork() ;

  if (pid == 0)
  {
	  execv(args[0], args);
      
  }else{
	  waitpid( pid,&checkfail,0 ) ;
	  if (WEXITSTATUS(checkfail) != 0)
	  {
		  GtkWidget *dialog = NULL;
		  dialog = gtk_message_dialog_new (GTK_WINDOW (win), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "Could not properly launch FFXIConfig.\n");
		  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
		  gtk_dialog_run (GTK_DIALOG (dialog));
		  gtk_widget_destroy (dialog);
	  }
    }
}


// About Menu Item
static void aboutit (GtkWidget *wid, GtkWidget *win)
{
  GtkWidget *dialog = NULL;
  sprintf(aboutlab,"%s", "FFXI-Nix\n\n""Version 0.1\n""is an application designed to make\n""ashita on nix easy to do.");
  dialog = gtk_message_dialog_new (GTK_WINDOW (win), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, aboutlab);
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

// Main Function
int main( int   argc, char *argv[] )
{

 // Build the main window
    gtk_init (&argc, &argv);
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "FFXI-Nix :: Wings XI"); 
    g_signal_connect (G_OBJECT (window), "delete_event", G_CALLBACK (delete_event), NULL);
    g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (destroy), NULL);
    gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (window), 2);

// Slap on a container
    vertbox = gtk_vbox_new (FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), vertbox);

// Menu
    menu = gtk_menu_bar_new();
    filemenu = gtk_menu_item_new_with_label("File");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), filemenu);
    aboutmenu = gtk_menu_item_new_with_label("About");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), aboutmenu);
    gtk_box_pack_start (GTK_BOX (vertbox), menu, TRUE, TRUE, 0);
    filesubmenu = gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(filemenu), filesubmenu);
    closeswitch = gtk_menu_item_new_with_label("Close");
    gtk_menu_shell_append(GTK_MENU_SHELL(filesubmenu), closeswitch);
    g_signal_connect(closeswitch, "activate", gtk_main_quit, NULL);
    gtk_container_add(GTK_CONTAINER(vertbox), menu);
    gtk_box_set_child_packing(GTK_BOX(vertbox), menu, FALSE, FALSE, FALSE, GTK_PACK_START);
    g_signal_connect(window, "destroy", gtk_main_quit, NULL);
    gtk_widget_realize(window);
    aboutsubmenu = gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(aboutmenu), aboutsubmenu);
    aboutswitch = gtk_menu_item_new_with_label("About FFXI-Nix");
    gtk_menu_shell_append(GTK_MENU_SHELL(aboutsubmenu), aboutswitch);
    g_signal_connect(aboutswitch, "activate", G_CALLBACK(aboutit), NULL);

// Text
    sprintf(textlab, "%s", "<big><b>FFXI-Nix</b></big>\n\n");
    textstuff = gtk_label_new (NULL);
    gtk_label_set_markup(GTK_LABEL (textstuff), textlab);
    gtk_label_set_justify(GTK_LABEL (textstuff),GTK_JUSTIFY_CENTER);
    gtk_misc_set_alignment (GTK_MISC (textstuff), 0, 1);
    gtk_box_pack_start (GTK_BOX (vertbox), textstuff, TRUE, TRUE, 0);
  

// Image
    image = gtk_image_new_from_file(".logo.jpg");
    gtk_box_pack_start(GTK_BOX(vertbox), image, FALSE, FALSE, 0);

// Launch Button
    pokeme = gtk_button_new_from_stock ("Launch");
    g_signal_connect (G_OBJECT (pokeme), "clicked", G_CALLBACK (launch), (gpointer) window);
    gtk_box_pack_start (GTK_BOX (vertbox), pokeme, TRUE, TRUE, 0);

// Config Button
    pokeme = gtk_button_new_from_stock ("Graphics");
    g_signal_connect (G_OBJECT (pokeme), "clicked", G_CALLBACK (config), (gpointer) window);
    gtk_box_pack_start (GTK_BOX (vertbox), pokeme, TRUE, TRUE, 0);

// FFXICONFIG
    pokeme = gtk_button_new_from_stock ("Config FFXI");
    g_signal_connect (G_OBJECT (pokeme), "clicked", G_CALLBACK (configxi), (gpointer) window);
    gtk_box_pack_start (GTK_BOX (vertbox), pokeme, TRUE, TRUE, 0);

// Shove it all together and make it go weeeeeeeeeee
    gtk_container_add (GTK_CONTAINER (window), vertbox);

    gtk_widget_show_all(window);

    gtk_main ();

    return 0;
}
